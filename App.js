import {StatusBar} from 'expo-status-bar';
import {StyleSheet, View, TextInput, Button, FlatList, Text, Modal} from 'react-native';
import {useState} from "react";
import {AddProduct} from "./components/AddProduct";
import {ProductItem} from "./components/ProductItem";
import {ListsToCheckChecked} from "./components/ListsToCheckChecked";
import uuid from 'react-native-uuid';
import {DeleteProduct} from "./components/DeleteProduct";


export default function App() {
    
    
    const initProducts = [
        {
            id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
            title: 'First Item',
            isChecked: false
        },
        {
            id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
            title: 'Second Item',
            isChecked: false
        },
        {
            id: '58694a0f-3da1-471f-bd96-145571e29d72',
            title: 'Third Item',
            isChecked: false
        },
    ];
    
    
    const [products, setProducts] = useState(initProducts);
    const [productInput, setProductInput] = useState(null);
    const [showAddModal, setShowAddModal] = useState(false);
    const [showDeleteModal, setShowDeleteModal] = useState(false);
    
    
    function changeTextHandler(input) {
        const id = uuid.v4();
        setProductInput({id: id, title: input, isChecked: false})
    }
    
    function productAddedHandler() {
        setShowAddModal(false);
        setProducts(prev => [...prev, productInput]);
    }
    
    function cancelProductAddedHandler() {
        setShowAddModal(false);
    }
    
    function productDeletedHandler(id) {
        setShowDeleteModal(false);
        setProducts(prev => prev.filter(prod => prod.id !== id));
    }
    
    function cancelProductDeletedHandler() {
        setShowDeleteModal(false);
    }
    
    function toggleCheckHandler(id) {
        const prods = products.map(prod => {
            if (prod.id === id) {
                prod.isChecked = !prod.isChecked;
                return prod;
            }
            return prod;
        });
        
        setProducts(prods);
        
    }
    
    return (
        <View style={styles.container}>
            
            <Button title="+ Ajouter" onPress={() => setShowAddModal(true)}/>
            
            
            <AddProduct
                visible={showAddModal}
                onChangeText={changeTextHandler}
                onCancel={cancelProductAddedHandler}
                onPress={productAddedHandler}/>
    
            <DeleteProduct
                visible={showDeleteModal}
                onCancel={cancelProductDeletedHandler}
                onPress={productDeleteddHandler}
            />
            
            <View style={styles.listContainer}>
                <ListsToCheckChecked allProducts={products} onToggleCheck={toggleCheckHandler}/>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 20
    },
    listContainer: {
        flex: 1,
        width: '100%',
        marginTop: 20,
    }
});
