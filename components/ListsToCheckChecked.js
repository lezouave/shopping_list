import {FlatList, StyleSheet, View} from "react-native";
import {ProductItem} from "./ProductItem";

export function ListsToCheckChecked({allProducts, onToggleCheck}) {
    const products = allProducts.filter(prod => prod && !prod.isChecked);
    const productsChecked = allProducts.filter(prod => prod && prod.isChecked);
    
    function toggleCheckHandler(id){
        onToggleCheck(id);
    }
    
    return(
        <View>
            <View style={styles.productsListToCheck}>
                <FlatList data={products} renderItem={itemData => <ProductItem title={itemData.item.title} onToggleCheck={toggleCheckHandler.bind(this, itemData.item.id)} isChecked={false}/>}/>
            </View>
         
            <View style={styles.productsListChecked}>
                <FlatList data={productsChecked} renderItem={itemData => <ProductItem title={itemData.item.title} onToggleCheck={toggleCheckHandler.bind(this, itemData.item.id)} isChecked={true}/>}/>
            </View>
    
       
        </View>
    )
}


const styles = StyleSheet.create({
   productsListToCheck: {
       marginBottom: 12,
   },
    productsListChecked: {
    }
});
